import React from 'react';

const SingleInput = (props) => (  
  <div className="single-input">
    <label className="form-label">{props.title}</label>
    <input
      className="text-input"
      value={props.content}
      onChange={props.controlFunc} />
    { !props.isValid ? <label className="error-label"> {props.errorMessage} </label> : null }
  </div>
);

export default SingleInput;