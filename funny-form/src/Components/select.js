import React from 'react';
import '../Css/form.css';

const Select = (props) => (  
  <div className="single-input">
    <label className="form-label">{props.title}</label>
    <select
      className="select-input"
      value={props.optionSelected}
      onChange={props.controlFunc}>
      <option value="none"></option>
      {props.options.map ((opt, i) => {
          return (
              <option key={i} value={opt.toLowerCase()}>{opt}</option>
            )
        })}
    </select>
    { !props.isValid ? <label className="error-label"> {props.errorMessage} </label> : null }
  </div>
);

export default Select;