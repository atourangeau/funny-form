import React from 'react'
import SingleInput from './singleInput'
import Select from './select'
import '../Css/animations.css'
import axios from 'axios'

const defaultSelectOption = "none"

export class Form extends React.Component{

    constructor(props){
        super(props);

        this.state = this.getInitialState();

        this.handleEmailChanged = this.handleEmailChanged.bind(this);
        this.handleFirstNameChanged = this.handleFirstNameChanged.bind(this);
        this.handleLastNameChanged = this.handleLastNameChanged.bind(this);
        this.handleGenderChanged = this.handleGenderChanged.bind(this);
        this.handleAgeChanged = this.handleAgeChanged.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.validate = this.validate.bind(this);
    }

    getInitialState(){
        return {
            email: "",
            isEmailValid: true,
            firstName: "",
            isFirstNameValid: true,
            lastName: "",
            isLastNameValid: true,
            gender: defaultSelectOption,
            isGenderValid: true,
            hasError: false,
            requestSubmitted: false,
            isInProgress: false,
            age: "",
            isAgeValid: true
        }
    }

    handleEmailChanged(e){
        this.setState({
            ...this.state,
            email: e.target.value
        });
    }
    
    handleFirstNameChanged(e){
        this.setState({
            ...this.state,
            firstName : e.target.value
        })
    }

    handleLastNameChanged(e){
        this.setState({
            ...this.state,
            lastName : e.target.value
        })
    }

    handleGenderChanged(e){
        this.setState({
            ...this.state,
            gender : e.target.value
        })
    }

    handleAgeChanged(e){
        this.setState({
            ...this.state,
            age: e.target.value
        })
    }

    handleSubmit(){
        if(!this.state.isInProgress & this.validate()){

            this.setState({
                ...this.state,
                isInProgress: true
            });
            
            axios.post('www.example.com', {
                email: this.state.email,
                firstName: this.state.firstName,
                lastName: this.state.lastName,
                gender: this.state.gender,
                age: parseInt(this.state.age)
            })
            .then(response => {
                // request has been successful
                var state = this.getInitialState();
                state.requestSubmitted = true;
                this.setState(state);
            })
            .catch(reason => {
                // request has failed
                // Right now it's doing the same thing as a success request scenario since we are using
                // a fake endpoint 
                var state = this.getInitialState();
                state.requestSubmitted = true;
                this.setState(state);
            })
        }
    }

    validate(){
        var state = {
            ...this.state,
            isGenderValid: !(this.state.gender === defaultSelectOption),
            isEmailValid: this.isEmailValid(this.state.email),
            isFirstNameValid: this.isNameValid(this.state.firstName),
            isLastNameValid: this.isNameValid(this.state.lastName),
            isAgeValid: this.isAgeValid(this.state.age),
            requestSubmitted: false
        };

        state = {
            ...state,
            hasError: !(state.isGenderValid && state.isEmailValid && state.isFirstNameValid && state.isLastNameValid && state.isAgeValid)  
        };

        this.setState(state, () => { setTimeout(() => this.setState({hasError: false}), 1000)});

        return !state.hasError;
    }

    isEmailValid(email){
        var pattern = /^([aA-zZ0-9-_][.]?)+@([aA-zZ])+.([aA-zZ]){2,3}$/;
        return pattern.test(email);
    }

    isNameValid(name){
        return !(name === "");
    }

    isAgeValid(age){
        var pattern =/^[0-9]{1,3}$/
        return pattern.test(age);
    }

    handleKeydown(e){
        console.log(e);
        if(e.keyCode === 13){
            this.handleSubmit()
        }
    }

    render() {
        return(
            <section className="form" onKeyDown={(e) => this.handleKeydown(e)}>
                <SingleInput title="Email" content={this.state.email} controlFunc={this.handleEmailChanged} 
                    isValid={this.state.isEmailValid} errorMessage="You must enter a valid email"/>
                <SingleInput title="First Name" content={this.state.firstName} controlFunc={this.handleFirstNameChanged} 
                    isValid={this.state.isFirstNameValid} errorMessage="You must enter a valid first name"/>
                <SingleInput title="Last Name" content={this.state.lastName} controlFunc={this.handleLastNameChanged} 
                    isValid={this.state.isLastNameValid} errorMessage="You must enter a valid last name"/>
                <SingleInput title="Age" content={this.state.age} controlFunc={this.handleAgeChanged}
                    isValid={this.state.isAgeValid} errorMessage="You must be enter a valid age" />
                <Select title="Gender" options={["Woman", "Man", "Other"]} optionSelected={this.state.gender} 
                    controlFunc={this.handleGenderChanged} isValid={this.state.isGenderValid} errorMessage="You must choose a gender" />
                <button className={"single-button " + (this.state.hasError ? "invalid-information" : "")} onClick={() => this.handleSubmit()} >Submit</button>
                { this.state.requestSubmitted ? <div className="confirmation-label">Request Submitted</div> : null }
            </section>
        )
    }
}